const express = require('express');
const router = express.Router();
const path = require('path');

// Define routes
router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../index.html'));
});

router.get('/insert', (req, res) => {
    res.sendFile(path.join(__dirname, '../insert.html'));
});

router.delete('/delete', (req, res) => {
    res.sendFile(path.join(__dirname, '../insert.html'));
});

module.exports = router;