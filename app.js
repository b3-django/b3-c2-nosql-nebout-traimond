const http = require('http');
const { MongoClient, ObjectId } = require('mongodb');
const route = require('./routes/route');
const express = require('express');
const app = express();

// Use the route middleware
app.use('/', route);

// Start the server
app.listen(3000, () => {
    console.log('Server is running on port 3000');
});


const uri = 'mongodb+srv://test:kenshiro@clusterethan.pxc6xsl.mongodb.net/';
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

async function disconnect() {
    try {
        await client.close();
        console.log('Disconnected from the MongoDB cluster');
    } catch (err) {
        console.error('Error disconnecting from the MongoDB cluster', err);
    }
}


async function insertData(data, collection) {
    const insertResult = await collection.insertOne(data);

    if (insertResult && insertResult.result && insertResult.result.ok === 1 && insertResult.insertedCount === 1) {
        console.log('Personne inserted failed');
    } else {
        console.log('Personne insertion Succesfull.');
        console.log('Insert Result:', insertResult);
        return insertResult.insertedId;
    }
}

async function deleteData(collection, filter) {
    const deleteResult = await collection.deleteOne(filter);
    if (deleteResult && deleteResult.deletedCount === 1) {
        console.log('Record deleted successfully!');
    } else {
        console.log('Record deletion failed.');
        console.log('Delete Result:', deleteResult);
    }
}

async function updateData(collection, filter, update) {
    const updateResult = await collection.updateOne(filter, update);
    if (updateResult && updateResult.modifiedCount === 1) {
        console.log('Record updated successfully!');
    } else {
        console.log('Record update failed.');
        console.log('Update Result:', updateResult);
    }
}

async function connect() {
    try {
        await client.connect();
        console.log('Connected to the MongoDB cluster');
        const collection = client.db('test').collection('mycollection');
        const newPersonne = {
            name: 'Jean',
            age: 20,
        };

        idInserted = await insertData(newPersonne, collection);
        const filter = { _id: idInserted };
        const update = { $set: { name: 'Eric', age: 100 } };
        await updateData(collection, filter, update);
        await deleteData(collection, filter);

        const documents = await collection.find({}).limit(20).toArray();

        console.log('All :');
        documents.forEach((Personne) => {
            console.log(Personne);
        });
    } catch (err) {
        console.error('Error connecting to the MongoDB cluster', err);
    } finally {
        await disconnect();
    }
}

// Call the connect function
connect();